# TODO for RTFM++

## Definitions
**Job:**

* A function that runs to completion in a finite time (no forever loop),
 not as a "normal" thread that has a forever loop.
* Has a settable priority which indicates the urgency of the Job.

**Resource:**

* An entity symbolizing something lockable, i.e. any locked Resource may
only be accessed by a single Job at a time.

**Lock (SRP):**

* A lock on a resource keeps other jobs, that will also take said resource,
from running through manipulation of the systems ISR settings. A lock can only
be held within a job and must be released before the exit of a job.


---

## Kernel primitives needing implementation:

### Job handling:
* `pend(Job)`
    * Request a job for direct execution
* `clear(Job)`
    * Remove pending job request
* `async(Job, ...)`
    * Pend a job after a prescribed time

### Resource handling:

**Stack Resource Policy (SRP):**

* `lock(Resource)`
    * Local lock (within a Job) of a Resource, i.e. the lock must be released
    before the end of the Job.
* `release(Resource)`
    * Release of local lock

**Per Resource Policy (PRP) extensions:**

* `boundary_lock(Resource)`
    * Lock between jobs, i.e. a lock which can live across Job executions.
* `boundary_release(Resource)`
    * Release of lock between jobs.

### Timer handling
* Re-implement the timers/queue for async, details in "Real-Time For the Masses: Abstract Timers and Their Implementation onto the ARM Cortex family of MCUs"
